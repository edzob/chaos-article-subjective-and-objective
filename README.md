# Chaos Paper

- Chaos is the result of the connections between systems.
- Chaos is also the result of not being able to make sense from observations.
- Chaos is defined as the (absolute) knowledge of the know provides any insight in the future.
- Chaos is also defined as the lack of correlation in the observations as also no causal relation between the observations.

This paper will make a clear distinction between "subjective" and "objective" chaos.

The goal is to have this paper published at an academic conference.

### Potential academic conferenced
1. IEEE CBI 2022
1. Springer EEWC 2022/2023
1. ANT 2022/2023

## Open Knowledge
This document will continuously be updated,
and is available for everyone to see and participate,
since I believe in open science, open access and open knowledge.

### Initial version
This document is (initially) created by me ([@edzob](https://www.edzob.com)) and 
published under the creative commons BY-SA 4.0 license. 
The content is a summary of available and open knowledge. Feedback is welcome. 

The source of this document is on 
[Gitlab](https://gitlab.com/edzob/chaos-article-subjective-and-objective) 
as an open repository.

## Related Work
*  2021-09-22 - "Design for Chaos" 
[Sogetilabs](https://labs.sogeti.com/design-for-chaos-a-dya-whitepaper-by-sogeti) 
white paper
*  2021-11-09 - "Attributes relevant to antifragile organizations"
[researchgate](https://www.researchgate.net/publication/354321606) 
[10.1109/CBI52690.2021.00017](http://dx.doi.org/10.1109/CBI52690.2021.00017) 
IEEE paper
*  2020-05-20 - "Defining Antifragility and the application on Organisation Design" 
[researchgate](https://www.researchgate.net/publication/343671048)
[10.5281/zenodo.3719388](https://doi.org/10.5281/zenodo.3719388) 
MSc thesis

---

## Run

To compile Latex continuous: 

```
make clean render LATEXMK_OPTIONS_EXTRA=-pvc; 
```

To refresh the PDF continuous:
```
evince paper/latexmk/main.pdf;
```

This runs the PDF viewer [Evince](https://wiki.gnome.org/Apps/Evince) 
that refreshes automatically when the pdf is changed. 

This is using 
[Docker](https://docs.docker.com/get-docker/) 
and 
[GNU make](https://www.gnu.org/software/make/)
together with 
[latexMK](https://ctan.org/pkg/latexmk?lang=en) 
in a the 
[texlive:latest container](https://hub.docker.com/r/texlive/texlive).

The [texlive:latest container](https://hub.docker.com/r/texlive/texlive).
is updated weekly by the texlive organisation.

---

## Acknowledgment

Thanks to [Martin Isaksson](https://cv.martisak.se/) 
with his great manual 
[How to annoy your co-authors: a gitlab CI Pipeline for LaTeX](https://blog.martisak.se/2020/05/11/gitlab-ci-latex-pipeline/) 
and for his [code-base](https://gitlab.com/martisak/latex-pipeline), that I [adjusted](https://gitlab.com/edzob/latex-pipeline/-/tree/texlive).


Thank you to [makeareadme.com](https://www.makeareadme.com) for the template for this Readme.

---

## Support to you
You can leave questions, remarks etc in the gitlab tracker, 
or just drop me ([Edzo](https://www.edzob.com/page/contact/)) a line.

## Roadmap
1. copy content from white paper
1. remove non essential content
1. rewrite reasoning to academic format.
1. re-evaluate reasoning
1. add the relevance of the reasoning (application and so what)

## Contributing
* Feel free to add or change content.
* Feel free to add comments via the tracker.

## LaTeX
The source of this document is/will be in LaTeX, so that everybody can add content in plain text,
and pdf's can be created in every way.

LaTeX has been around since ever, so there are many many resources out there how to start.
There is for example a coursera course on latex.
Many universities, all over the world, offer their 1-year students a course in LaTeX.

Still not certain where to start. Just drop me ([Edzo](https://www.edzob.com/page/contact/)) a line.

## Authors
* Edzo Botjes

## License
This project is licenced under [Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/).
